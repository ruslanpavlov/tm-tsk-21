package ru.tsc.pavlov.tm.command.task;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return TerminalConst.TASK_REMOVE_BY_INDEX;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Remove task by index";
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!getTaskService().existsByIndex(userId, index)) throw new TaskNotFoundException();
        getTaskService().removeByIndex(userId, index);
        System.out.println("[OK]");
    }

}
