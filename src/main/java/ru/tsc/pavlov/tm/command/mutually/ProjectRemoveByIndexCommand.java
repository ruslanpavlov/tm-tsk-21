package ru.tsc.pavlov.tm.command.mutually;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.exception.empty.EmptyIndexException;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class ProjectRemoveByIndexCommand extends AbstractMutuallyCommand {

    @Override
    public String getName() {
        return TerminalConst.PROJECT_REMOVE_BY_INDEX;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Remove project by index";
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER PROJECT INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (index == null || index < 0) throw new EmptyIndexException();
        getProjectTaskService().removeByIndex(userId, index);
        System.out.println("[OK]");
    }

}
