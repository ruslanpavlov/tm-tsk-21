package ru.tsc.pavlov.tm.command.user;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.model.User;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractUserCommand{
    @Override
    public String getName() {
        return TerminalConst.USER_CHANGE_PASSWORD;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Changing password for user";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSPHRASE:");
        final String password = TerminalUtil.nextLine();

        final User user = getUserService().findByLogin(login);
        user.setPassword(password);
        System.out.println("PASSPHRASE CHANGED");
        showUser(user);
    }

}
