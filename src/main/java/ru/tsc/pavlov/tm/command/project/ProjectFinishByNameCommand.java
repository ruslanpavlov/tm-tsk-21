package ru.tsc.pavlov.tm.command.project;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.pavlov.tm.model.Project;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class ProjectFinishByNameCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return TerminalConst.PROJECT_FINISH_BY_NAME;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Start project by name";
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = getProjectService().finishByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
    }

}
