package ru.tsc.pavlov.tm.command.user;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.exception.entity.UserNotFoundException;
import ru.tsc.pavlov.tm.model.User;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class UserUpdateByLoginCommand extends AbstractUserCommand {
    @Override
    public String getName() {
        return TerminalConst.USER_UPDATE_BY_LOGIN;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "User update by login";
    }

    @Override
    public void execute() {
        System.out.println("PLEASE ENTER USER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER USER FIRST NAME:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("ENTER USER LAST NAME:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("ENTER USER MIDDLE NAME:");
        final String middleName = TerminalUtil.nextLine();
        System.out.println("ENTER PASSPHRASE:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER USER E-MAIL:");
        final String email = TerminalUtil.nextLine();

        final User user = getUserService().updateByLogin(login, lastName, firstName, middleName, email);
        if (user == null) throw new UserNotFoundException(login);
        else showUser(user);
        System.out.println("USER UPDATED");
    }

}
