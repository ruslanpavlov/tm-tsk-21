package ru.tsc.pavlov.tm.command.task;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.pavlov.tm.model.Task;
import ru.tsc.pavlov.tm.util.TerminalUtil;

public class TaskFinishByIdCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return TerminalConst.TASK_FINISH_BY_ID;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Finish task by id";
    }

    @Override
    public void execute() {
        final String userId = getAuthService().getCurrentUserId();
        System.out.println("[ENTER ID]");
        final String id = TerminalUtil.nextLine();
        final Task task = getTaskService().finishById(userId, id);
        if (task == null) throw new TaskNotFoundException();
    }

}
