package ru.tsc.pavlov.tm.repository;

import ru.tsc.pavlov.tm.api.repository.IUserRepository;
import ru.tsc.pavlov.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        return list.stream()
                .filter(u -> u.getLogin().toLowerCase().equals(login.toLowerCase()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return list.stream()
                .filter(u -> u.getEmail().toLowerCase().equals(email.toLowerCase()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        remove(user);
        return user;
    }

}
