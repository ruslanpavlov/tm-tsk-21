package ru.tsc.pavlov.tm.model;

import ru.tsc.pavlov.tm.api.entity.IWBS;
import ru.tsc.pavlov.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public class Task extends AbstractOwnerEntity implements IWBS {

    private Status status = Status.NOT_STARTED;

    private String name;

    private String description;

    private String projectId = null;

    private Date startDate;

    private Date created = new Date();

    private String userId;

    public Task() {

    }

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getProjectId() {
        return projectId;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(Date created) {
        this.created = created;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

}
