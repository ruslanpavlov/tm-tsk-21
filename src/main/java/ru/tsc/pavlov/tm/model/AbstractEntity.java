package ru.tsc.pavlov.tm.model;

import java.util.UUID;

public abstract class AbstractEntity {

    public String id = UUID.randomUUID().toString();

    public String getId() {
      return id;
    }

    public void setId(String id) {
      this.id = id;
    }

}
