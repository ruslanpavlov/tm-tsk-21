package ru.tsc.pavlov.tm.model;

import ru.tsc.pavlov.tm.api.entity.IWBS;
import ru.tsc.pavlov.tm.enumerated.Status;

import java.util.Date;

public class Project extends AbstractOwnerEntity implements IWBS {

    public Project() {
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    private String name;

    private Status status = Status.NOT_STARTED;

    private String description;

    private Date startDate;

    private Date created = new Date();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return id + ": " + name;
    }

    @Override
    public Date getCreated() {
        return created;
    }

    @Override
    public void setCreated(final Date created) {
        this.created = created;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }

}
